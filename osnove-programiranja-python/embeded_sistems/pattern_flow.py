# Na pocetku se vrti poruka na pisplayu: "Pritisnite srednji gumb za pocetak igre"
# Kad igrac to ucini igra pocne

# Upale se random piksel na displayu. Nakon 1 s upali se drugi i nakon jos jedne upali se treci.
# Nkon 3 sekunde svi pixeli se zagase, a potom se upali jedan pixel u gorenjem lijevom kutu.

# Igrac pomocu joysticka pomice pixel iz gornjeg lijevo kuta na poziciju na kojoj misli da je bio upaljeni pixel
# te pritisne srednji gumb ya obradu.
# Ako je pixel ispravno odabran zasvjetli zeleno, a igrac nastavlja do sljedeceg pixela. 
# Ako je pixel neispravno odabran zasvjetli crveno, a non nekoliko igraa se resetira na pocetni ekran

# Ako igrac tocno pogodi sve pixele disply yasvjetli zeleno, 
# a nakon nekoliko sekundi se igra restira na pocetni ekran.

from sense_emu import SenseHat
import random
import time

# constants
sense = SenseHat()

hello_msg = "Pritisnite joystick"

COLORS = {
    "yellow": (204, 204, 0),
    "blue": (0, 0, 204),
    "green": (0, 204, 0),
    "red": (204, 0, 0), 
}

#game variables
game_stared = False
game_over = False
game_won = False
pixels = []
guessed_pixels = []
game_pixel = {
    'x': 0,
    'y': 0,
}

game_len = 3

def check_pixel(game_pixel, pixels, guessed_pixels, game_len):
    game_over = True

    # for each pixel in pixels list check if it equals playes pixel
    # if it doesnt the game is over
    # if it does remember the guess
    # 2 pixels are the same ix thay have the same x and y valuse
    for pixel in pixels:
        x = int(pixel % 8)
        y = int(pixel / 8)
        if(x == game_pixel['x'] and y == game_pixel['y']):
            game_over = False
            if pixel not in guessed_pixels:
                guessed_pixels.append(pixel)
    # if all pixels are guessed the game is done
    game_won = len(guessed_pixels) >= game_len

    return game_over, game_won

#color the screen 3 times in the color given
def color_all(C):
    all_pixels = [
        C, C, C, C, C, C, C, C,
        C, C, C, C, C, C, C, C,
        C, C, C, C, C, C, C, C,
        C, C, C, C, C, C, C, C,
        C, C, C, C, C, C, C, C,
        C, C, C, C, C, C, C, C,
        C, C, C, C, C, C, C, C,
        C, C, C, C, C, C, C, C,
    ]
    for i in range(3):
        sense.set_pixels(all_pixels)
        time.sleep(0.5)
        sense.clear()
        time.sleep(0.5)

# set all variables to init state
def restart_game():
    game_stared = False
    game_over = False
    game_won = False
    pixels = []
    guessed_pixels = []
    game_pixel = {
        'x': 0,
        'y': 0,
    }
    game_len = 3
    
    return(
        game_pixel, 
        game_over, 
        game_stared, 
        game_won, 
        pixels, 
        guessed_pixels,
        game_len
    )

def extande_game(game_len):
    game_stared = True
    game_over = False
    game_won = False
    pixels = []
    guessed_pixels = []
    game_pixel = {
        'x': 0,
        'y': 0,
    }
    game_len = game_len + 1
    
    return(
        game_pixel, 
        game_over, 
        game_stared, 
        game_won, 
        pixels, 
        guessed_pixels,
        game_len
    )

while True:
    if not game_stared:
            # show hello message
        sense.show_message(hello_msg)

        # check if user clicked midle button
        events = sense.stick.get_events()
        for event in events:
            if event.action == 'released':
                if event.direction == 'middle':
                    print("game has stared")
                    game_stared = True
    
    else: # game has stared

        # get random pixels -> get 3 random ints 0-63
        # only if there are no pixels
        if len(pixels) < game_len:
            while len(pixels) < game_len:
                pixel = random.randint(0, 63)
                if pixel not in pixels:
                    pixels.append(pixel)

            # show pattern to player -> clear screeen show pixel by pixel
            sense.clear()
            for pixel in pixels:
                time.sleep(1)
                x = int(pixel % 8) # broj % broj_stupaca = koji smo stupac
                y = int(pixel / 8) # broj / broj_redaka = koji smo redak
                sense.set_pixel(x, y, COLORS['yellow'])

            # wait for 3 s, then show 
            time.sleep(3)
            sense.clear()
            # show game pixel -> clear
            sense.set_pixel(game_pixel['x'], game_pixel['y'], COLORS["blue"]) 

        # control game pixel
        event = sense.stick.wait_for_event()
        if event.action == 'released':
            if event.direction == 'up':
                game_pixel['y'] = game_pixel['y'] - 1
                game_pixel['y'] = max(0, game_pixel['y']) # check if out of bound
            if event.direction == 'down':
                game_pixel['y'] = game_pixel['y'] + 1
                game_pixel['y'] = min(7, game_pixel['y'])
            if event.direction == 'left':
                game_pixel['x'] = game_pixel['x'] - 1
                game_pixel['x'] = max(0, game_pixel['x'])
            if event.direction == 'right':
                game_pixel['x'] = game_pixel['x'] + 1
                game_pixel['x'] = min(7, game_pixel['x'])
            if event.direction == 'middle':
                # check if evaluatio is true/false
                print('checking pixels')
                game_over, game_won = check_pixel(game_pixel, pixels, guessed_pixels, game_len)
        
        # show new game_pixel position -> clear old positon, show new
        sense.clear()
        for pixel in guessed_pixels:
            x = int(pixel % 8)
            y = int(pixel / 8)
            sense.set_pixel(x, y, COLORS['green'])
        sense.set_pixel(game_pixel['x'], game_pixel['y'], COLORS['blue'])

        # game over
        if game_over:
            color = COLORS['red']
            color_all(color)
            sense.clear()
            msg = f'MAX: {game_len}'
            sense.show_message(msg)
            (game_pixel, game_over, game_stared, game_won, pixels, guessed_pixels, game_len) = restart_game()

        # game won
        if game_won:
            color = COLORS['green']
            color_all(color)
            (game_pixel, game_over, game_stared, game_won, pixels, guessed_pixels, game_len) = extande_game(game_len)

