from sense_emu import SenseHat

sense = SenseHat()
X = (255, 0, 0)
O = (0, 255, 0)
question_mark = [
O, O, O, X, X, O, O, O,
O, O, X, O, O, X, O, O,
O, O, O, O, O, X, O, O,
O, O, O, O, X, O, O, O,
O, O, O, X, O, O, O, O,
O, O, O, X, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, X, O, O, O, O
]
sense.set_pixels(question_mark)
# sense.show_message("Hello, Sense HAT!", text_colour=color)
