from sense_emu import SenseHat
import time 

sense = SenseHat()


#EKRAN

msg = "Bilo koji string"
# sense.show_message(msg)

#uple za bolju (red, green, blue) sve vrijednosti su an skali 0-255
p = (0, 0, 255)
z = (200, 200, 0)
# sense.show_message(
#     msg, 
#     text_colour= plava,
#     scroll_speed= 0.002,
#     back_colour= zuta,
#     )

# sense.show_letter('A')

pixeli = [
    p,p,p,p,p,p,p,p,
    p,p,p,z,z,z,z,p,
    p,p,p,z,z,z,z,p,
    p,z,z,z,z,z,z,p,
    p,z,z,z,z,z,z,p,
    p,z,z,z,z,z,z,p,
    p,z,z,z,z,z,z,p,
    p,p,p,p,p,p,p,p,
]
kutevi = [0, 90, 180, 270]
# sense.set_pixels(pixeli)
# for kut in kutevi:
#     time.sleep(1)
#     sense.set_rotation(kut)

# sense.flip_h()
# sense.flip_v()

# CITANJE VRIJEDNOSTI SENYORA

temp = sense.get_temperature()
print(temp)
vlaynost = sense.get_humidity()
print(vlaynost)
tlak = sense.get_pressure()
print(tlak)
msg = f'Temp = {temp}, vlg = {vlaynost}, tlak = {tlak}'
# sense.show_message(msg)

# kut u stupljevima oko x, y, z osi
roll, pitch, yaw = sense.get_orientation().values()
print(roll, pitch, yaw)
# ubrizanja od -1 do 1 g, u x, y, z
# x, y, z  = sense.get_accelerometer_raw().values()

# JOYSTICK
# print('Poruka prije')
# event = sense.stick.wait_for_event()
# event.action # pressed ili relesd
# event.direction # up, down, left, right, middle
# print('Poruka nakon')
# while True:
#     # print('Prouka prije')
#     events = sense.stick.get_events()
#     for event in events:
#         if event.action == 'pressed':
#             print("Pritisnut je:")
#             if event.direction == 'up':
#                 print('za gore')
#             if event.direction == 'down':
#                 print('za doel')
#             if event.direction == 'left':
#                 print('za lijevo')
#             if event.direction == 'right':
#                 print('za desno')
#             if event.direction == 'middle':
#                 print('sredina')
#         elif event.action == 'released':
#             print("otpustena je")
#             if event.direction == 'up':
#                 print('za gore')
#             if event.direction == 'down':
#                 print('za doel')
#             if event.direction == 'left':
#                 print('za lijevo')
#             if event.direction == 'right':
#                 print('za desno')
#             if event.direction == 'middle':
#                 print('sredina')
#     # print("Poruak nakon")

w = (255, 255, 255)
b = (0, 0, 0)
up_arrow = [
    b,b,b,b,b,b,b,b,
    b,b,b,w,w,b,b,b,
    b,b,w,w,w,w,b,b,
    b,w,w,w,w,w,w,b,
    b,b,b,w,w,b,b,b,
    b,b,b,w,w,b,b,b,
    b,b,b,w,w,b,b,b,    
    b,b,b,b,b,b,b,b,
]
sense.set_pixels(up_arrow)
while True:
    # print('Prouka prije')
    events = sense.stick.get_events()
    for event in events:
        if event.action == 'pressed':
            print("Pritisnut je:")
            if event.direction == 'up':
                print('za gore')
                sense.set_pixels(up_arrow)
                sense.set_rotation(0)
            if event.direction == 'down':
                print('za doel')
                sense.set_pixels(up_arrow)
                sense.set_rotation(180)
            if event.direction == 'left':
                print('za lijevo')
                sense.set_pixels(up_arrow)
                sense.set_rotation(270)
            if event.direction == 'right':
                print('za desno')
                sense.set_pixels(up_arrow)
                sense.set_rotation(90)
            if event.direction == 'middle':
                print('sredina')
                sense.clear()
# def wait_for_event():
#     while True:
#         events = sense.stick.get_events()
#         events2 = controler.stick2.get_events()
#         event = events[0]
#     return event

# event = contoler.wait_for_event()
# event.action
# event.direction
# event.stick # "left", "right"
