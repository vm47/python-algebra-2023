def tijelo_while():
    print('Prvi broj je veci')
    print('Pokusajte unijeti nova dva broja')
    prvi_broj = int(input())
    drugi_broj = int(input())
    return prvi_broj, drugi_broj

def ponavljaj(a = 5, b = 1):
    while a > b:
        a, b = tijelo_while()
    zelis_ponovit = input('Unesite n ako zelis prekinuti izvodenje')
    return zelis_ponovit
# WHILE -> kad zelimo da se nesto ponavlja dok se neki uvijet ne izvrsi

zelis_ponovit = 'y'

while zelis_ponovit.casefold() != 'n':
    zelis_ponovit = ponavljaj()

