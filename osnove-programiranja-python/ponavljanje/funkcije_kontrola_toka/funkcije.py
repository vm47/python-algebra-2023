# # definiranje same funkcije
# # kljucna rijec def
# # ime 
# # unutar zagrada, imena argumenta (argumenti su varijable)

# # blok naredbi koji se izvrsava mora biti uvucen za 1 tab 

# # return koji vraca verijabu (ili vise njih)
# def foo(a, b):
#     c = a + b
#     d = c * 2
#     e = d + a
#     return e, d

# def ispis(za_print):
#     print(f'Ovo je vas "za print": {za_print}')
#     return # ne vracamo nsita pa mozemo izostavit return

# #poziv funkcije
# # funkcije pozivamo preko imena u zagrade stavimo argumente koji postoje 
# # ili pustimo prazne zagrade inace
# # rezulatat mozemo spremiti u varijablu ako je to potrebno
# a, b = foo(5, 6)
# e, d = foo(22, 55)
# rez_dva = foo(a, 2)
# ispis(rez_dva)


def input_and_add():
    a = int(input())
    b = int(input())
    rez = a + b
    print(f'Reyultat unutar funkcije {rez}')
    return rez

nasa_varijabla = input_and_add()
nasa_varijabla += 10
print(f'Nasa varijabla ali u glavnom progamu {nasa_varijabla}')

input_and_add()