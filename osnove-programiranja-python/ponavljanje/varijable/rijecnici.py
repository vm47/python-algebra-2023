# DICT RIJECNIK MAPA HESH 
# "lista elementa" ALI umjesto indexa imaju kljuceve
# KLJUC MORA BITI UNIKATAN
# ima CRUD

#create
rijecnik = {'kljuc1': 'vrijednost1'}
#print(rijecnik)
rijecnik.update({'kljuc2': 'vrijednost2'}) #dodavanje ALI samo ako kljuc ne postoji
#print(rijecnik)

#update
rijecnik.update({'kljuc2' : 'nova_vrijednost'})
print(rijecnik)

#read
print(rijecnik['kljuc1'])
print(rijecnik.get('kljuc1', 'No value'))

#nested update
rijecnik.update({'kljuc3': [1,2,3,4,5]})
rijecnik['kljuc3'].append(6)
print(rijecnik)

#delete
rijecnik.pop('kljuc3')
print(rijecnik)