import pickle

try:
    with open('data.bin', 'wb') as file:
        data = {
            'ime': "Igor"
        }
        pickle.dump(data, file)
except Exception as e:
    print(e)