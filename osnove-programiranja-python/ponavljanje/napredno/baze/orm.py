# ORM -> object relation mapping

from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import sessionmaker, declarative_base

Base = declarative_base()

class Osoba(Base):
    __tablename__ = 'osoba'
    id = Column(Integer, primary_key = True)
    ime = Column(String)
    prezime = Column(String)


# spojimo se na bazu
engine = create_engine('sqlite:///Baza.db')
Session = sessionmaker(bind=engine)
session = Session()
# izvrsimo upit
#read
data = session.query(Osoba).all()
data = session.query(Osoba).filter_by(ime = "Igor")
# create
osoba = Osoba(ime = 'Igor', prezime = 'Vukas')
session.add(osoba)
session.commit() # moramo dodat commit kada pisemo u bazu

#update -> read + izmejan
osoba2 = session.query(Osoba).filter_by(id=1).first()
osoba2.ime = "Novo ime"
session.commit()

#delete -> read + delete
osoba = session.query(Osoba).filter_by(id=1).filter()
session.delete(osoba)
session.commit()

# odposjimo se
# NE MORAMO KAD RADIMO OVAKO S BAZOM

