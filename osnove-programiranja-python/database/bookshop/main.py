from sqlalchemy import create_engine
from models.base import Base
from services.book_service import BookService
from services.author_service import AuthorService
from services.publisher_service import PublisherService
from presentation.screens.main_screen import MainScreen


DB_NAME = 'bookshop.db'
DATABASE_URL = f'sqlite:///{DB_NAME}'

def main():
    engine = create_engine(DATABASE_URL)
    Base.metadata.create_all(engine)

    book_service = BookService(engine)
    author_service = AuthorService(engine)
    publisher_service = PublisherService(engine)

    main_screen = MainScreen(book_service, author_service, publisher_service)
    main_screen.show_menu()

if __name__ == '__main__':
    main()

