from models.publisher import Publisher
from sqlalchemy.orm import sessionmaker


class PublisherService:
    def __init__(self, engine):
        self.engine = engine
        self.Session = sessionmaker(bind=self.engine)

    def create_publisher(self, name, year_opened):
        session = self.Session()
        publisher = Publisher(name=name, year_opened=year_opened)
        session.add(publisher)
        session.commit()

    def get_all_publishers(self):
        session = self.Session()
        return session.query(Publisher).all()

    def get_publisher_with_books(self, publisher_id):
        session = self.Session()
        return session.query(Publisher).filter(Publisher.id == publisher_id).first()
