from models.author import Author
from sqlalchemy.orm import sessionmaker


class AuthorService:
    def __init__(self, engine):
        self.engine = engine
        self.Session = sessionmaker(bind=self.engine)

    def create_author(self, first_name, last_name):
        session = self.Session()
        author = Author(first_name=first_name, last_name=last_name)
        session.add(author)
        session.commit()

    def get_all_authors(self):
        session = self.Session()
        return session.query(Author).all()

    def get_author_with_books(self, author_id):
        session = self.Session()
        return session.query(Author).filter(Author.id == author_id).first()
