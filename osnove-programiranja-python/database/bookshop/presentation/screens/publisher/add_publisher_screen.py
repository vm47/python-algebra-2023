class AddPublisherScreen:
    def __init__(self, publisher_servise):
        self.publisher_service = publisher_servise

    def add_publisher(self):
        name = input("Enter the publisher's name: ")
        year_opened = int(input("Enter the year the publisher opened: "))

        self.publisher_service.create_publisher(name, year_opened)
        print("Publisher added successfully!")