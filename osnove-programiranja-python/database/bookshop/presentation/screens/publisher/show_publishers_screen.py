class ShowPublisherScreen:
    def __init__(self, publisher_service):
        self.publisher_service = publisher_service

    def show_publisher_and_book(self):
        publishers = self.publisher_service.get_all_publishers()
        for publisher in publishers:
            print(f"{publisher.id}. {publisher.name}")

        publisher_id = int(input("Enter the ID of the publisher: "))

        publisher = self.publisher_service.get_publisher_with_books(publisher_id)
        if publisher:
            print(f"\nPublisher: {publisher.name}")
            for book in publisher.books:
                print(f"Book: {book.name}, Price: ${book.price}")
        else:
            print("Publisher not found.")
    
    def show_publishers(self):
        publishers = self.publisher_service.get_all_publishers()
        for publisher in publishers:
            print(publisher.name)