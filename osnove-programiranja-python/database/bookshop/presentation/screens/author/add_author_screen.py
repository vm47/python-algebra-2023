class AddAuthorScreen:
    def __init__(self, author_service):
        self.author_service = author_service

    def add_author(self):
        first_name = input("Enter the author's first name: ")
        last_name = input("Enter the author's last name: ")

        self.author_service.create_author(first_name, last_name)
        print("Author added successfully!")
