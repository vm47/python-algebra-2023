class ShowBookScreen:
    def __init__(self, book_service):
        self.book_service = book_service

    def show_books(self):
        books = self.book_service.get_all_books()
        for book in books:
            print(book.name)
    
    def show_book_details(self):
        book_id = int(input("Enter the ID of the book: "))

        book = self.book_service.get_book_by_id(book_id)
        if book:
            print(f"\nBook: {book.name}")
            print(f"Price: ${book.price}")
            print(f"Is Available: {book.is_available}")
            print(f"Author: {book.author.first_name} {book.author.last_name}")
            print(f"Publisher: {book.publisher.name}")
        else:
            print("Book not found.")
