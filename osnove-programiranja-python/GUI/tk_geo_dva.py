import tkinter as tk

root = tk.Tk()
root.title('Naslov')


# 3x3 grid
# GRID -> lak za radit veritilakno i paraletno istovremeno, koricno za brzo slagane kompleksnih GUI-a
# komprosim izmedu pack, i placa
for i in range(3):
    root.columnconfigure(i, weight=1, minsize=75)
    root.rowconfigure(i, weight=1, minsize=75)

    for j in range(3):
        frame = tk.Frame(
            root,
            relief=tk.RAISED,
            borderwidth=1
        )
        frame.grid(
            row=i, 
            column=j, 
            padx=15, 
            pady=20
        )

        label = tk.Label(
            frame,
            text=f'Red {i} stupac {j}'
        )

        label.pack()


root.mainloop()