import tkinter as tk
from tkinter.ttk import Checkbutton
from tkinter import messagebox
from services.user_service import UserService

class SmartKeyApp:
    def __init__(self, user_service: UserService) -> None:

        self.current_pin = ''
        self.current_satus = 'Statusi i poruke\n'

        # dohvatit bazu i popunit self.logs s vrijednostima iz iste
        self.user_service = user_service

        self.root = tk.Tk()
        self.root.title("SmartKey")
        self.root.geometry('600x400')


        # init_panel,
        self.init_panel = tk.Frame(self.root)
        self.init_panel.pack() 

        self.ring_doorbell_button = tk.Button(
            self.init_panel, 
            text="Pozvoni!",
            command=self.ring_doorbell
        )

        self.ring_doorbell_button.grid(
            row=0,
            column=0,
            padx=5,
            pady=5,
        )

        self.unlock_door_button = tk.Button(
            self.init_panel, 
            text="Odkljucaj!",
            command=self.unlock_door
        )

        self.unlock_door_button.grid(
            row=0,
            column=1,
            padx=5,
            pady=5
        )


         
        # input_pin, 
        self.input_pin = tk.Toplevel(self.root) # vezemo na root da zamjeni ekran
        self.input_pin.title("Input Pin") # dodatmo novi naslov
        self.input_pin.geometry('600x400')
        self.input_pin.withdraw()

        self.control_frame = tk.Frame(self.input_pin)

        self.label_frame = tk.Frame(self.control_frame)

        self.pin_label = tk.StringVar()
        self.pin_label.set('Prikaz slova')

        self.pin_label_show = tk.Label(
            self.label_frame,
            textvariable=self.pin_label,
            font=('Segoe UI', 24),
            fg='red'
            )

        self.pin_label_show.pack()

        self.label_frame.pack()


        self.keypad_frame = tk.Frame(self.control_frame)


        number = 1
        for i in range(3):

            for j in range(3):
                frame = tk.Frame(
                    self.keypad_frame,
                    relief=tk.RAISED,
                    borderwidth=1
                )
                frame.grid(
                    row=i, 
                    column=j, 
                    padx=10, 
                    pady=10
                )

                button = tk.Button(
                    frame,
                    text=f'{number}',
                    command= lambda n= number: self.keypad_clicked(n)
                )

                button.pack()

                number += 1

        frame = tk.Frame(
            self.keypad_frame,
            relief=tk.RAISED,
            borderwidth=1
        )
        frame.grid(
            row=i+1, 
            column=0, 
            padx=10, 
            pady=10,
            columnspan= 3
        )

        button = tk.Button(
            frame,
            text=f'Done',
            command= self.check_pin
        )

        button.pack()
            
        self.keypad_frame.pack()

        self.control_frame.pack(side=tk.LEFT)


        self.satus_frame = tk.Frame(self.input_pin)

        self.satus_label = tk.StringVar()
        self.satus_label.set(self.current_satus)

        self.satus_label_show = tk.Label(
            self.satus_frame,
            textvariable=self.satus_label,
            font=('Segoe UI', 24),
            fg= 'blue'
            )

        self.satus_label_show.pack()

        self.satus_frame.pack(side=tk.RIGHT)
        # admin_screen
        self.admin_screen = tk.Toplevel(self.root) # vezemo na root da zamjeni ekran
        self.admin_screen.title("Admin Screen") # dodatmo novi naslov
        # self.admin_screen.geometry('600x400')
        self.admin_screen.withdraw()

        self.admin_user_list_frame = tk.Frame(self.admin_screen)
        #Add list
        self.users_listbox = tk.Listbox(self.admin_user_list_frame, width=50)
        self.users_listbox.pack(side=tk.LEFT, padx=10, pady=10)
        #make list clicable
        self.users_listbox.bind('<<ListboxSelect>>', self.fill_admin_form)
        #fill list
        self.fill_user_listbox()
        self.admin_user_list_frame.grid(row=0, column=0)
        
        
        self.admin_input_form_frame = tk.Frame(self.admin_screen)
        self.name_entry = self.sk_input_field(self.admin_input_form_frame, "Ime")
        self.l_name_entry = self.sk_input_field(self.admin_input_form_frame, "Prezime")
        self.pin_entry = self.sk_input_field(self.admin_input_form_frame, "Pin")
        self.activity_checkbox = self.sk_checkbox(self.admin_input_form_frame, "Aktivan")
        self.admin_qta = self.sk_qta(self.admin_input_form_frame)
        self.admin_input_form_frame.grid(row=0, column=1)


    def ring_doorbell(self):
        print('Ring_doorbell funciton entered')
        messagebox.showinfo('Ring Ring', 'Netko ce vam uskoro otvoriti vrata!')
        return
    
    def unlock_door(self):
        print('Unlock_door function entered')
        self.input_pin.deiconify() # priakzi datelje
        self.root.withdraw() 
        return

    def keypad_clicked(self, number):
        print(f"Keypad_clicked with {number}")
        self.current_pin += str(number)
        self.pin_label.set(self.current_pin)
        return

    def check_pin(self):
        print(f"check_pin enterd with current_pin {self.current_pin}")
        pin = self.current_pin
        self.current_pin = ''
        self.pin_label.set(self.current_pin)
        #provjerit dal pin postoji
        self.current_user = self.user_service.get_user_by_pin(pin= int(pin))
        if(self.current_user == None):
            print("Pin ne postoji")
            self.current_satus += str("Pin ne postoji\n")
            self.satus_label.set(self.current_satus)
        # ispisat odgovoracujucu poruku
        # navigirat admina, ako je admin u pitanju
        elif(self.current_user.first_name == "ADMIN"):
            print('Wellcome ADMIN')
            self.current_satus += str('Wellcome ADMIN\n')
            self.satus_label.set(self.current_satus)
            self.open_admin_screen()
        else:
            print(f'Dobrodosli {self.current_user.first_name}')
            self.current_satus += str(f'Dobrodosli {self.current_user.first_name}\n')
            self.satus_label.set(self.current_satus)
        return


    def open_admin_screen(self):
        print("open_admin_screen entered")
        self.admin_screen.deiconify() # priakzi datelje
        self.input_pin.withdraw()
        self.root.withdraw() 
        return

    def fill_user_listbox(self):
        self.users = self.user_service.get_all_users()
        for user in self.users:
            self.users_listbox.insert(tk.END, f'{user.first_name} {user.last_name}')


    def sk_input_field(self, parent, label_text, show=None):
        
        frame = tk.Frame(parent)
        frame.pack(padx=10, pady=10, fill=tk.X)

        label = tk.Label(frame, text=label_text, width=20)
        label.pack(side=tk.LEFT)

        entry = tk.Entry(frame, show=show)
        entry.pack(
            side=tk.LEFT, 
            padx=5, 
            pady=0, 
            expand=True, 
            fill=tk.X
        )

        return entry 


    def sk_checkbox(self, parent, label_text):
        
        frame = tk.Frame(parent)
        frame.pack(padx=10, pady=10, fill=tk.X)

        label = tk.Label(frame, text=label_text, width=20)
        label.pack(side=tk.LEFT)

        entry = Checkbutton(frame)
        entry.pack(
            side=tk.LEFT, 
            padx=5, 
            pady=0, 
            expand=True, 
            fill=tk.X
        )

        return entry 

    def sk_qta(self, parent):
        frame = tk.Frame(parent)
        frame.pack(padx= 10, pady= 10, fill= tk.X)

        save_button = tk.Button(
            frame,
            text='Spremi',
            command=self.save_user_cta
        )
        save_button.grid(row=0, column=0)
        clear_button = tk.Button(
            frame,
            text='Odustani',
            command=self.clear_admin_form_cta
        )
        clear_button.grid(row=0, column=1)
        delete_button = tk.Button(
            frame,
            text='Izbrisi',
            command=self.delete_user_cta
        )
        delete_button.grid(row=0, column=2)
        return frame

    def fill_admin_form(self, event):
        selection = self.users_listbox.curselection() # doynajemo koji element je odabran
        if selection:
            name = self.users_listbox.get(selection[0])
            f_name, l_name = name.split()
            self.entry = self.user_service.get_user_by_name(f_name, l_name)

            self.name_entry.setvar(self.entry.first_name)
            self.l_name_entry.setvar(self.entry.last_name)
            self.pin_entry.setvar(self.entry.pin)
            self.activity_checkbox.setvar(self.entry.is_active)
        # get user
        # fill values
        return 

    def save_user_cta(self):
        print("Save user cta entered")
        name =  self.name_entry.get().strip()
        l_name = self.l_name_entry.get().strip()
        pin = self.pin_entry.get().strip()
        is_active = self.activity_checkbox.state()

        if (self.user_service.add_user(name, l_name, pin, ('selected' in is_active))):
            messagebox.showinfo('Uspijeh', 'Password je dodan uspjesno!')

            self.clear_admin_form_cta()
        
        else:
            messagebox.showerror("ERROR", "Mora postojati ime zapisa!")
        return
    
    def clear_admin_form_cta(self):
        print("Clear admin form cta enterd")
        self.name_entry.delete(0, tk.END)
        self.l_name_entry.delete(0, tk.END)
        self.pin_entry.delete(0, tk.END)
        self.activity_checkbox.deselect()
        return

    def delete_user_cta(self):
        print("Delete user cta entered")
        if(self.entry.id != None and self.entry.id != None):
            self.user_service.delete_by_id(self.entry.id)
        return

    def run(self):
        self.root.mainloop()