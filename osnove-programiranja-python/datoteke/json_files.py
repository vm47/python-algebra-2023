import json
# import pickle

# citanje
try:
    with open('data.json', 'r') as file:
        data = json.load(file) # ekvivalent file.read() za json
    print(data)
    #print(data['hobiji'][0])
except Exception as e:
    print(f"There was an error {e}")

# pisanje

class Osoba:
    def __init__(self, 
        ime: str, 
        godine: str, 
        prebivaliste: str
    ) -> None:
        self.ime = ime
        self.godine = godine
        self.prebivaliste = prebivaliste

    def to_json(self) -> dict:
        return {
            'ime': self.ime,
            'godine': self.godine,
            'prebivaliste': self.prebivaliste
        }
    
    def from_json(data: dict):
        return Osoba(
            ime= data['ime'],
            godine= data['godine'],
            prebivaliste= data['prebivaliste']
        )
        return Osoba(**data)

# osoba_marko = Osoba('Marko', '25', 'Zagreb')

# try:
#     with open('write_data.json', 'w') as file:
#         json.dump(osoba_marko.to_json(), file, indent= 4) # ekvivalent file.write(data) za tekts
# except Exception as e:
#     print(f'There was an error: {e}')

try:
    with open('write_data.json', 'r') as file:
        data = json.load(file)
    osoba = Osoba.from_json(data= data)
    print(osoba.to_json())
except Exception as e:
    print(e)

