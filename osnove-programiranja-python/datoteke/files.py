# otvori datoteku
# obradi podatke
# zatvori datoteku

# read (za citanje), 
# write (za pisanje pise od pocetka) i 
# append (isto za pisanje ovo pise na kraj)

# ima vise tipova datoteka trenutno tekstualne i 
# json (koje su isto tekstualne ali posebne)

# apslutna (u donosu pa pocetak diska(root)) 
# i relativna (u odnostu na trenutni file) putanja


# file = open('./imena.txt', 'w')

# file = open('./imena.txt', 'a')
# file.seek(0)
# isto sto i write seek mice na pocetak

file = open('imena.txt', 'a')

ime = input("Unesite svoje ime\t")
# print(ime)
file.write(f'{ime}\n')
# file.writelines(ime)
# input()

file.close()

###file###
###^   ### write
###   ^### append

file = open('imena.txt', 'r')

foo = file.readlines()

file.close()

for line in foo:
    print(line)
# print(foo)

# file.close()

file = open('brojevi', 'a')

for i in range(20):
    # file = open('brojevi', 'a')
    file.write(f'{i}\n')
    # file.close()

file.close()

with open('imena.txt', 'r') as file:
    data = file.read()

print(data)

# Otvaramo najkasnije razumno, a zatavarmo najranije razumno

# 0xfefff header <
# 0xff001 00010001010
# 0Xff002 00010101010
# 0xff003 10101011111
