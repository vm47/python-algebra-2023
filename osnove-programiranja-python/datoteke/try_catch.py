try:
    with open('imena.txt', 'r') as imena:
        data = imena.readlines
    # for line in data:
    #     print(line)
except FileNotFoundError:
    print('Neeeeema')
except Exception as e:
    print(e)
else:
    for line in data:
        print(line)
    # odvrti ako nema exeptiona
finally:
    pass
    # data.close()
    # odvrti uvijek i na kraju

# try:
#     for line in data:
#         print(line)
# except Exception as e:
#     print(e)

try:
    # code that may raise an exception
    x = 10 / 0  # Raises a ZeroDivisionError
except ZeroDivisionError as e:
    # handle the specific exception
    print("Error:", e)
except Exception as e:
    # handle other exceptions
    print("Unknown error:", e)
else:
    # code to execute if no exception occurs
    print("No exception occurred.")
finally:
    # code that always executes
    print("Cleanup resources.")