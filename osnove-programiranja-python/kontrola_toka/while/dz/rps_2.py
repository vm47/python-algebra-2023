#kamen papir skare alt
import random
komp_win = 0
igrac_win = 0
nerijeseno = 0
partija_count = 0
pocetak = input ("Zelis li zapoceti igru y/n: ").casefold()
komp_pobjeda = [["kamen", "skare"], ["skare", "papir"], ["papir", "kamen"]]
da = ["y", "yes", "yup"]

while pocetak in da:
    lista_rezult = []
    lista_izbor = ["kamen", "papir", "skare",]
    komp_izbor = random.choice (lista_izbor)
    lista_rezult.append (komp_izbor)
    igrac_izbor = input("Odaberi kamen, papir ili skare: ").casefold()
    
    while igrac_izbor not in lista_izbor:
        igrac_izbor = input("Krivi unos, odaberi kamen, papir ili skare: ").casefold()
    
    lista_rezult.append (igrac_izbor) 
    
    if komp_izbor == igrac_izbor:
        print ("Igra je nerijesena")
        nerijeseno +=1
    elif lista_rezult in komp_pobjeda:
        print ("Komp je pobijedio")
        komp_win +=1
    else:
        print ("Tvoja pobjeda")
        igrac_win += 1
    partija_count += 1
    print (f"Trenutni rezultat je: od {partija_count} odigranih, pobijedio si {igrac_win}, izgubio {komp_win}, nerijesenih je bilo {nerijeseno}")
    pocetak = input ("Zelite li ponoviti igru y/n: ").casefold()