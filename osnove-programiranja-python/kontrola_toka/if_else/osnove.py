uvijet = False # uvijet mora biti bool
if uvijet:
    print("Hello from if")
# print("Prosi smo if")

if uvijet: # ako je uvijet TRUE
    print("Hello from if")
else: # ako je uvijet FALSE ce se izvrsit
    print("Hello from else")

# x = 7 #int(input(''Unesite broj))
# uvijet = x > 5
# print(type(uvijet)) #Boolean

# kad pricamo o brojevima svi brojevi osim 0 se gledaju kao TRUE
# x = 0 
# imas 0 i 1, False i True, 
# if x == 5:
#     print('Hello world')

# kad pricamo o sringovima sve osim praznog stringa (' ') je TRUE 

#ZAD: napisati program u kojem korisnik kaze svoju dob, 
# a vi provijerite i ispisete je li korisnik punoljetan ili ne
dob = int(input('Unesite svoju dob:\t'))
if dob < 18:
    print('Vi ste maloljetni')
else: 
    print('Mozete glasati')