rijec = input('Upsite sto yelite provjeriti za palindrom:\t')
rijec_strip = rijec.replace(' ', '').casefold() # pirema stringa case sasitive i miacanje razmaka
obrnuta_rijec = rijec_strip[::-1]

# # je_palindrom = True
# for i in range(len(rijec)): #len(rijec)//2
#     if(rijec[i] != rijec[-(i+1)]): # index 0 je prvi index -1 je zadnji, index 1 je drugi, index -2 je predzadnji
#         # je_palindrom = False # za svaki par indexa provjera ako postoji par koji se ne podudara
#         print('Nije paliindrom')
#         break
# else:
#     print('je palindrom')

if(rijec_strip == obrnuta_rijec):
    print(f'Unos {rijec} je palindrom') 
else:
    print(f'Unos {rijec} nije palindrom')