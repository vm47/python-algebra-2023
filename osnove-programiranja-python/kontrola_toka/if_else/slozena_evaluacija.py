# trenutna_godina = 2023
# godiste = int(input('Unesite godiste:\t'))
# if (trenutna_godina - godiste) >= 18:
#     print('Mozete glasata')

# A B   A AND B   A OR B    NOT A (!A)
# T T      T         T        F
# T F      F         T        F
# F T      F         T        T
# F F      F         F        T

#ZAD: korisnik unosi mjesec i godinu rodenja (2 inputa)
# provjerite moze li glasat
# GODINA = 2023
# MJESEC = 4
# godiste = int(input('Unesite godiste:\t'))
# mj_rod = int(input('Unesite mj rodenja:\t'))
# #moze glasata ako ima vise od 18, ako ima 18(treba provjeriti mjesec)
# if ((GODINA - godiste) > 18) or ((GODINA - godiste) == 18 and (mj_rod <= MJESEC)):
#     print("Mozete glasati")
# else:
#     print("Maloljetni ste")


# uvijet1 = False
# uvijet2 = True
# uvijet3 = True

# if uvijet1:
#     print('Prvi uvijet je tocan')
# elif uvijet2:
#     print('Drugi uvijet je tocan')
# elif uvijet3:
#     print('Treci uvijet je tocan')
# else:
#     print("oba su netocna")

#ZAD: osoba unosi godine
# provjeravamo, je li radno sposobna (16), umirovljenik (65), ili dijete
# dob = int(input('Unesite svoju dob:\t'))
# if dob >= 65:
#     print('Vi ste u mirovini')
# elif dob >= 16:
#     print('Vi ste radno sposobni')
# else:
#     print('Vi ste jos dijete')

# #ZAD: prvijerite li upisana godina prijestupna
# # googlajte prijestupnu godinu nije samo ako je djeljivo s 4
# # mora bit djeliva s 4, ali nesmije sa 100, uz izuzetak 400
# godina = int(input('Unesite godinu:\t'))
# #for godine in range(9999):
# if (godina % 4 == 0):
#     if(godina % 100 == 0):
#         if(godina % 400 == 0):
#             print(f"godina {godina} je prijestupna")
#         else:
#             print(f"godina {godina} nije prijestupna")
#     else:
#         print(f"godina {godina} je prijestupna")
# else:
#     print(f"godina {godina} nije prijestupna")

# if (godina % 400 == 0): # if 0: 
#     print(f"godina {godina} je prijestupna")
# elif (godina % 100 == 0):
#     print(f"godina {godina} nije prijestupna")
# elif godina % 4 == 0:
#     print(f"godina {godina} je prijestupna")
# else:
#     print(f"godina {godina} je prijestupna")

# if (godina % 400 == 0) or (godina % 4 == 0  and godina % 100 != 0):
#     print(f"godina {godina} je prijestupna")
# else:
#     print(f"godina {godina} nije prijestupna")

# uvijet = (godina % 400 == 0) or (godina % 4 == 0  and godina % 100 != 0)
# print(f"godina {godina} je prijestupna") if uvijet else print(f"godina {godina} nije prijestupna")
# (izvrsi ovo) if (uvijet) else (izvrsni nesto drugo)

#ZAD: za uneseni broj projverite je li prim broj (primarni/prosti) (to je broj koji je dijeljiv samo s 1 i samim sobom)
broj = int(input('Unesite neku broj'))
is_prime = True
for i in range(2, broj): #range 500 0-499 sqrt -> drugi korijen iz broja
    # print("Testing")
    if(broj % i == 0):
        is_prime = False
        # break
if is_prime:
    print(f'Broj {broj} je prim broj')
else:
    print(f'Broj {broj} nije prim broj')
