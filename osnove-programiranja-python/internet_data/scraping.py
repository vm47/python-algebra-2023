import urllib.request as u_req
import urllib.parse as u_parse
import requests

URL = 'https://www.algebra.hr'

konekcija = u_req.urlopen(URL)

stranica = konekcija.read().decode()

# print(stranica)

upit = {'q': "programiranje u pythonu"}

upit_e = u_parse.urlencode(upit)
upit_e_utf8 = upit_e.encode('utf-8')

GOOGLE_URL = f'https://www.google.com/search?{upit_e}'

# print(GOOGLE_URL)
# print(upit_e_utf8)

req = u_req.Request(GOOGLE_URL)
req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3')

res = u_req.urlopen(req)

data = res.read().decode()
# print(data)

URL = 'https://www.algebra.hr'
res = requests.get(URL)
print(res.status_code)
# print(res.content)
print(res.headers)
print(res.text)