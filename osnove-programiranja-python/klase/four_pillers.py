# Abstrakcija -> jednu klasu koja nam je glavna (bez detalja)
# Enkapsulaacija -> sve sto se moze s nekom klasom bi trebao bit dio nje
# Nasljedivanje -> jedna klaasa moze naslijedit drugu, i time preuzet njene dijelove
# Polimorfizam -> bilo koju klasu djete mozemo koristit umijesto roditelja

from abc import ABC, abstractmethod #ABC abstraktna bazna klasa
# import abc

class Shape(ABC):
    @abstractmethod
    def getArea(self):
        pass
    
    @abstractmethod
    def getPerimeter(self):
        pass

    # def draw(self):
    #     print(self.getArea())

class Circle(Shape):
    def __init__(self, radius):
        self.__radius = radius
    
    def getArea(self):
        return 3.14 * self.__radius ** 2
    
    # def getPerimeter(self):
    #     return 2 * 3.14 * self.__radius

class Rectangle(Shape):
    def __init__(self, width, height):
        self.__width = width
        self.__height = height
    
    def getArea(self):
        return self.__width * self.__height
    
    def getPerimeter(self):
        return 2 * (self.__width + self.__height)

# Inheritance example
class Square(Rectangle):
    def __init__(self, side):
        # self.__rect = Rectangle(side, side)
        super().__init__(side, side)

# Polymorphism example
def drawShape(shape):
    print("Area:", shape.getArea())
    print("Perimeter:", shape.getPerimeter())

circle = Circle(5)
rectangle = Rectangle(3, 4)
square = Square(2)


drawShape(circle) # Output: Area: 78.5, Perimeter: 31.4
drawShape(rectangle) # Output: Area: 12, Perimeter: 14
drawShape(square) # Output: Area: 4, Perimeter: 8


# abstraktna klasa i sucelje