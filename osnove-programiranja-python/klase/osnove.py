# osoba = {
#     'ime': '',
#     'prezime': '',
# }
# def ispis_pozdrava(osoba):
#     print(f'Pozdrav {osoba['ime']}')

# osoba = {
#     'ime': "",
#     'prezime': "",
#     'oib': 1,
# }

class Bike():
    def __init__(self, max_brzina, tezina, nosivost, is_electric, ime_bike) -> None:
        self.ime = ime_bike
        self.tezina = tezina
        self.nosivost = nosivost
        self.is_electric = is_electric
        self.max_speed = self.get_true_max(max_brzina)

    def get_true_max(max_brzina):
        if(max_brzina > 100):
            return 100
        elif(max_brzina < 100 and max_brzina > 20):
            return max_brzina
        else:
            return max_brzina + 10

    def vozi(self):
        print(f'vozi bicklu {self.ime}')

    def stani(self):
        pass

class Osoba:
    # definitari sto sve opisuje jednu osobju

    # boja kose
    #boja ocuju
    # visina
    # tezina
    # ime
    # preyime
    # oib
    def __init__(self, visina, tezina, ime, prezime):
        self.ime = ime
        self.prezime = prezime
        self.visina = visina
        self.tezina = tezina
        self._has_bike = False
        self.bike = None
        while True:
            oib = input(f"Unesite oib za osobu {ime}")
            if(len(oib) <= 11):
                self.oib = oib
                break
            else:
                print("Ne moze biti taj oib")
        
    def dodaj_biciklu(self, bike):
        self._has_bike = True
        self.bike = bike

    def drive_bike(self, bike):
        print(f"OSoba {self.ime}", end=' ')
        bike.vozi()

    # sto sve ta osoba moze ili sto se s njom moze

    # moze ju se podravit
    def poz_osobu(self):
        print(f"Pozdrav {self.ime} {self.prezime}")

    # osoba moze hodati
    def hodaj(self):
        print(f'Osoba {self.oib} je krenula hodati')

    # govoriti
    def reci_frazu(self, fraza):
        print(f'Osoba {self.ime} kaze: {fraza}')
    # ...
    def zbrajaj(self, prvi_broj, drugi_broj):
        zbroj = prvi_broj + drugi_broj
        print(f'Osoba {self.prezime} kaze da je zbroj {prvi_broj} i {drugi_broj} {zbroj}')

    def bmi(self):
        bmi = self.tezina / (self.visina) ** 2
        print(f"Osoba {self.ime} ima bmi od: {bmi}")

    def set_ime(self, ime):
        self.ime = ime
# def osoba(visinu, tezinu , ime, prezime, oib):
    # print(f"{ime}")

# max_speed, tezina, nosivost, is_electric, ime
antin_bike =  Bike(30, 50, 556, True, "Antina Bicika")

# visina tezine ime prezime oib
marko = Osoba(654, 1250, "Marko", "Divovski")
# marko.set_ime("ime")

marko.drive_bike(antin_bike)
# marko.dodaj_biciklu(antin_bike)
# marko.bike.vozi()

# osoba(1,2,"a", "b", "12")

marko.poz_osobu()

marko.hodaj()

marko.reci_frazu("Bok svima")

marko.zbrajaj(5, 10)

pero = Osoba(54, 12, "Pero", "Sitni")

pero.poz_osobu()

pero.bmi()

# nas_set = ['asdf', 'bef'] # list()
# list()

# nas_set.append()
# len(nas_set[0])