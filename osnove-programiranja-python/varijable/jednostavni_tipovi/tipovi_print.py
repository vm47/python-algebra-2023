ime = "Igor" #sting lista znakova char[] = ['I', 'g', 'o', 'r']
prezime = 'Vukas' #string
datu_rodenja = '01.01.1995.' #string
dan = 1 #number
mj = 1 #integer
godina_rodenja = 1995
zaposlenje = 'Zaposlen'
foo = True #boolean
foo_too = 5 > 2 # True
# print(foo_too)
# print(ime[:3])
# print(prezime)
# print(dan, mj, godina_rodenja)

mikorvalna_trosi_kW = 1.3
kosite_h_dnevbo = 2
dana_u_mj = 30
tarifa_e_kw = 0.07

potrosnja = mikorvalna_trosi_kW * kosite_h_dnevbo * dana_u_mj * tarifa_e_kw
# print(potrosnja)

stranica_cm = 5
visina_cm = 4
povrsina_trokuta_cm = stranica_cm * visina_cm / 2
print(f'Pravsina je \'{povrsina_trokuta_cm}\' cm^2')


# jedna linija komentara
# ? adsfas
'''
vise linija komentara
drugu liniju
trecu
'''
a = 5
b = 10
povrsina = a * b
print("povrsina", povrsina)