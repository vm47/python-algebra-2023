# zapisat ljestivicu
tonovi = ['C', 'C#', 'D', 'D#', 'E', 'F', 'G', 'G#', 'A', 'A#', 'H']
tonovi += tonovi
# ton = input korisnika predpostavka da korisnik unosi postojeci ton
pocetni_ton = input('Unesite pocetni ton   ')
# iyracunat durski akord ton, ton + 4, ton + 7
index_pt = tonovi.index(pocetni_ton)
durski_dva = tonovi[index_pt + 4]
durski_tri = tonovi[index_pt + 7]
# izracunati molski akord: ton, ton + 3, ton + 7
molski_dva = tonovi[index_pt + 3]
molski_tri = tonovi[index_pt + 7]
# ispisati korisniku njegove akorde
print(f'Durski akord je: {pocetni_ton} {durski_dva} {durski_tri}')
print(f'Molski akord je {pocetni_ton} {molski_dva} {molski_tri}')